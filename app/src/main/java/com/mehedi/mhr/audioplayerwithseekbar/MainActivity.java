package com.mehedi.mhr.audioplayerwithseekbar;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    SeekBar seekAudio;
    Button btnBack, btnPlayPause, btnFor;
    MediaPlayer mp;
    Runnable runnable;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekAudio = (SeekBar)findViewById(R.id.sb_activity_main_seek);
        btnBack = (Button)findViewById(R.id.btn_activity_main_back);
        btnFor = (Button)findViewById(R.id.btn_activity_main_for);
        btnPlayPause = (Button)findViewById(R.id.btn_activity_main_pause);

        mp = MediaPlayer.create(getApplicationContext(),R.raw.audiofile);
        handler = new Handler();

        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                seekAudio.setMax(mp.getDuration());
                mp.start();
                changeSeekbar();


            }
        });

        btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp.isPlaying()){
                    mp.pause();
                    btnPlayPause.setText(">");
                }
                else {

                    mp.start();
                    btnPlayPause.setText("||");
                    changeSeekbar();
                }
            }
        });

        btnFor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.seekTo(mp.getCurrentPosition()+5000);
                changeSeekbar();

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.seekTo(mp.getCurrentPosition()-5000   );
                changeSeekbar();
            }
        });


        seekAudio.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (fromUser){

                    mp.seekTo(progress);

                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void changeSeekbar(){

        seekAudio.setProgress(mp.getCurrentPosition());
        if (mp.isPlaying()){

            runnable = new Runnable() {
                @Override
                public void run() {

                    changeSeekbar();
                }
            };

            handler.postDelayed(runnable,100);
        }

    }
}
